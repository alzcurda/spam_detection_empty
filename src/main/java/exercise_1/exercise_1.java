package exercise_1;


import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.ml.classification.LinearSVC;
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator;
import org.apache.spark.ml.feature.HashingTF;
import org.apache.spark.ml.feature.Tokenizer;
import org.apache.spark.ml.param.ParamMap;
import org.apache.spark.ml.tuning.CrossValidator;
import org.apache.spark.ml.tuning.CrossValidatorModel;
import org.apache.spark.ml.tuning.ParamGridBuilder;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import static org.apache.spark.sql.types.DataTypes.*;

public class exercise_1 {

	// Crea un hashmap a partir del fichero de pares (ID_Mail, label)
	public static HashMap<String, Integer> loadSampleLabels(String filePath)
	{
		HashMap<String, Integer> map = new HashMap<>();
		String linea;
		String[] sLinea;
		try {
			BufferedReader br = new BufferedReader(new FileReader(filePath));
			while ((linea = br.readLine()) != null){
				//System.out.print(linea);
				sLinea = linea.split(",");
				map.put(sLinea[0],Integer.parseInt(sLinea[1]));
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return(map);
	}
	
	public static Dataset<Row> readMailFiles(SparkSession ss,
											JavaSparkContext jsc, String path)
	{
		Dataset<Row> emailsDataset;
		// ToDo: Crear un dataset con dos columnas: fileName y text con los ficheros
		// existentes en path.
		StructType schema = new StructType(new StructField[]{
				createStructField("filename",StringType,true),
				createStructField("text",StringType,true)
		});
		JavaPairRDD<String,String> allEmails = jsc.wholeTextFiles(path);
		JavaRDD<Row> emailsRDD = allEmails.map(tuple -> RowFactory.create(tuple._1(),tuple._2()));
		emailsDataset = ss.createDataFrame(emailsRDD, schema);

		return(emailsDataset);
	}
	
//	public static Dataset<Row> labelMails(Dataset<Row> mails,
//										  Broadcast<HashMap<String, Integer>> bdHashMap)
//	{
//		Dataset<Row> labeledMails;
//		// Definimos un patrón de RE para buscar el ID en el nombre del fichero
//		final Pattern pattern = Pattern.compile(".*TRAIN_([0-9]*).*");
//
//		// ToDo: Añadir al dataset una nueva columna "label" obteniendo el file_id
//		// a partir del nombre del archivo y indexar con el el hashmap de labels para
//		// obtener el valor 0 o 1
//		labeledMails = mails;
//		return(labeledMails);
//	}

	// Transforma el vector de palabras al modelo TF_IDF
	public static Dataset<Row> transformTFIDF (Dataset<Row> ds, int numFeatures)
	{
		Tokenizer tokenizer = new Tokenizer().setInputCol("text").setOutputCol("words");

		Dataset<Row> wordsMail = tokenizer.transform(ds);

		HashingTF hashingTF = new HashingTF()
				.setInputCol("words")
				.setOutputCol("features")
				.setNumFeatures(numFeatures);

		return(hashingTF.transform(wordsMail));
	}

	// Ajusta un modelo SVM lineal mediante CV seleccionando el mejor parámetro C
	public static CrossValidatorModel fitModel(Dataset<Row> train)
	{
		LinearSVC lsvc = new LinearSVC()
				.setMaxIter(5)
				.setLabelCol("label")
				.setFeaturesCol("features");

		ParamMap[] paramGrid = new ParamGridBuilder()
				.addGrid(lsvc.regParam(), new double[] {10.0,1.0,0.1})
				.build();
		CrossValidator cv = new CrossValidator()
				.setEstimator(lsvc)
				.setEvaluator(new MulticlassClassificationEvaluator()
						.setMetricName("accuracy")
						.setLabelCol("label")
						.setPredictionCol("prediction"))
				.setEstimatorParamMaps(paramGrid).setNumFolds(5);

		return(cv.fit(train));
	}

	public static void spamDetection(SparkSession ss) {


		// Obtenemos el spark context
		JavaSparkContext jsc = new JavaSparkContext(ss.sparkContext());

		// leemos el fichero de nombres y labels con formato (fileId,label) y creamos un hasmap
		HashMap<String, Integer> labels = loadSampleLabels("src/main/resources/1_spam-mail.tr.label");

		// Utilizamos broadcast para enviar de forma eficiente el hashmap a los workers
		final Broadcast<HashMap<String, Integer>> bc = jsc.broadcast(labels);

		// Definimos un patrón de RE para buscar el ID en el nombre del fichero
		final Pattern pattern = Pattern.compile(".*TRAIN_([0-9]*).*");

		// Leemos sobre un dataset todos los ficheros de train
		Dataset<Row> eMails = readMailFiles(ss,jsc,"src/main/resources/1_TR");
	    
		// Añadimos una nueva columna "label" al dataset que ya tenemos con los nombres de archivo y contenidos
		StructType schema = new StructType(new StructField[]{
				createStructField("fileName",StringType,true),
				createStructField("text",StringType,true),
				createStructField("label",FloatType,true)
		});


		Dataset<Row> labeledMails = ss.createDataFrame(eMails.javaRDD().map(line ->
		{
			Matcher mClass = pattern.matcher(line.getString(0));
			float label = 0;
			if (mClass.find()) {
				String mailID = mClass.group(1);
				label = bc.value().get(mailID);
			}
			Row newRow = RowFactory.create(line.getString(0), line.getString(1), label);
			return (newRow);
		}), schema);

		labeledMails.show(3);
	    
	    // Aplicamos al texto del dataset el modelo TD_IDF para obtener una nueva variable "features" que contiene el vector TF_IDF
	    Dataset<Row> featurizedData = transformTFIDF (labeledMails, 1000);

	    // Dividimos el dataset en train i test
	    Dataset<Row>[] splits= featurizedData.randomSplit(new double[] {0.3,0.7});
	    Dataset<Row> train = splits[1];
	    Dataset<Row> test = splits[0];

		// Aseguramos permanencia del train en la memoria de los workers si es posible
		train.persist();

	    // Ajustamos un modelo de clasificación binaria con CV
	    CrossValidatorModel cvModel = fitModel(train);

		// Predicciones sobre test set
		Dataset<Row> predictions = cvModel.transform(test).select("prediction","label");

		// Definimos un evaluador
		MulticlassClassificationEvaluator evaluator = new MulticlassClassificationEvaluator()
				.setMetricName("accuracy")
				.setLabelCol("label")
				.setPredictionCol("prediction");

		double accuracy = evaluator.evaluate(predictions);
		System.out.println("Train samples: "+train.count());
		System.out.println("Test samples: "+test.count());
		System.out.println("Test Error = " + (1 - accuracy));

	    ss.stop(); 
	}
}
