import org.apache.spark.sql.SparkSession;
import exercise_1.exercise_1;

// Launch exercise
public class UPCSchool_MLlib {
	public static void main(String[] args) throws Exception {
		SparkSession spark = SparkSession.builder().master("local[*]")
				.appName("spamDetection")
				.getOrCreate();
		exercise_1.spamDetection(spark);
		spark.close(); 
	}

}
